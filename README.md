# Kubernetes Game 2048

Game is stateless and no volumes are hooked to manifest. 
Consist of only 2 files - deployment and svc. 
> Image is based on alpine. Source code can be found in the links  

```
# kubectl apply -f 0-2048-deployment.yaml
# Kubectl apply -f 1-2048-svc.yaml 
```
#### Important link 
* [Docker file repo ](https://www.github.com/alexwhen/docker-2048)
* [Source Code of Game](https://github.com/gabrielecirulli/2048)
